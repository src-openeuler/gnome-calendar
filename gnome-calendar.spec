%global libical_version 3.0.5
%global gsettings_desktop_schemas_version 3.21.2
%global edataserver_version 3.45.1
%global glib2_version 2.67.5
%global gtk4_version 4.6.0

Name:           gnome-calendar
Version:        44.1
Release:        1
Summary:        A simple and beautiful calendar application for GNOME.
License:        GPLv3+
URL:            https://wiki.gnome.org/Apps/Calendar
Source0:        https://download.gnome.org/sources/%{name}/44/%{name}-%{version}.tar.xz

BuildRequires:  gcc gettext gtk-doc meson pkgconfig(geocode-glib-1.0) 
BuildRequires:  pkgconfig(libdazzle-1.0) 
BuildRequires:  pkgconfig(libhandy-1) 
BuildRequires:  pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gsettings-desktop-schemas) >= %{gsettings_desktop_schemas_version}
BuildRequires:  pkgconfig(gtk4) >= %{gtk4_version}
BuildRequires:  pkgconfig(gweather4)
BuildRequires:  pkgconfig(libadwaita-1)
BuildRequires:  pkgconfig(libecal-2.0) >= %{edataserver_version}
BuildRequires:  pkgconfig(libedataserver-1.2) >= %{edataserver_version}
BuildRequires:  pkgconfig(libgeoclue-2.0)
BuildRequires:  pkgconfig(libical) >= %{libical_version}
BuildRequires:  pkgconfig(libsoup-3.0)
BuildRequires:  libappstream-glib
BuildRequires:  desktop-file-utils

Requires:       evolution-data-server%{?_isa} >= %{edataserver_version}
Requires:       glib2%{?_isa} >= %{glib2_version}
Requires:       gsettings-desktop-schemas%{?_isa} >= %{gsettings_desktop_schemas_version}
Requires:       gtk4%{?_isa} >= %{gtk4_version}
Requires:       libical%{?_isa} >= %{libical_version}

%description
GNOME Calendar is a simple and beautiful calendar application for GNOME. it gives
a lot of attention to details, and as such, design is an essential and ongoing
effort.

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%meson
%meson_build

%install
%meson_install
%find_lang %{name}

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/org.gnome.Calendar.desktop
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/org.gnome.Calendar.appdata.xml

%files -f %{name}.lang
%doc NEWS README.md
%license COPYING
%{_bindir}/gnome-calendar
%{_datadir}/applications/org.gnome.Calendar.desktop
%{_datadir}/dbus-1/services/org.gnome.Calendar.service
%{_datadir}/icons/hicolor/scalable/apps/org.gnome.Calendar*.svg
%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.Calendar*-symbolic.svg
%{_datadir}/metainfo/org.gnome.Calendar.appdata.xml
%{_datadir}/glib-2.0/schemas/org.gnome.calendar.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.calendar.gschema.xml
# co-own these directories
%dir %{_datadir}/gnome-shell
%dir %{_datadir}/gnome-shell/search-providers
%{_datadir}/gnome-shell/search-providers/org.gnome.Calendar.search-provider.ini

%changelog
* Fri Nov 24 2023 lwg <liweiganga@uniontech.com> - 44.1-1
- update to version 44.1

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.1-1
- Update to 43.1

* Mon Jun 20 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.2-1
- Update to 42.2

* Mon Jun 13 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.1-1
- Update to 42.1

* Mon Apr 18 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.38.2-3
- Add gnome-calendar.yaml

* Tue Jul 27 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-2
- Remove unnecessary "%" in Requires

* Wed Jun 16 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-1
- Package init with version 3.38.2
- Add '84.patch' that "avoid crashing when year view's weather icon is NULL"
- Add 4-4-gnome-calendar-update-to-3.38.1-add-libhandy-1.x-support.patch
